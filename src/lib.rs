use std::{
    fs::File,
    io::{self, Error, ErrorKind},
    marker::PhantomData,
    num::NonZeroU32,
    os::{fd::AsRawFd, unix::prelude::FileExt},
    path::Path,
    process, str,
};

use nix::{
    errno::errno,
    fcntl::{fcntl, FcntlArg},
    libc::{flock, kill, ESRCH, F_WRLCK, SEEK_SET},
};

/// A filesystem and pid based lock.
///
/// When you call [`acquire`](Self::acquire) with a path, the current thread
/// blocks until it has acquired an exclusive lock on that file and written
/// its process id (pid) to it.  Dropping the [`PidLock`] has the same effect
/// as calling [`release`](Self::release), which releases the filesystem
/// lock and clears the pidfile. By design, the pidfile is never deleted,
/// only truncated to be empty.
///
/// # Safety
/// This lock has a couple of limitations due to how filesystem locks work:
/// - The lock is provided on a best-effort basis. It may, for example, not work on NFS.
/// - There must never be multiple [`PidLock`]s referring to the same file in one process.
/// - This struct is not thread-safe.
#[derive(Debug)]
pub struct PidLock {
    fd: File,
    /// the pid of the current process, or [`None`] once [`release_impl`](Self::release_impl) was called
    pid: Option<NonZeroU32>,
    /// [`PidLock`] is not send and not sync
    phantom: PhantomData<*const ()>,
}

impl PidLock {
    /// Wait to acquire the lock and report any errors that occur.
    ///
    /// See [the struct level documentation](self) for more.
    pub fn acquire(path: impl AsRef<Path>) -> io::Result<PidLock> {
        let pid = NonZeroU32::new(process::id()).expect("process ids are never zero");

        // make sure we only ever create one file at the given path
        // this makes the following timeline impossible
        //   1. Process A creates pidfile and locks it
        //   2. Process B waits to acquire the lock on this file
        //   3. Process A unlinks the file and releases its lock
        //   4. Process B acquires the now unlinked and unlocked file
        //   5. Process C sees that there is no pidfile at the given path
        //   6. Process C creates a new and distinct pidfile and locks it
        //   7. Processes B and C both think they're holding the lock
        let fd = match File::options()
            .read(true)
            .write(true)
            .create_new(true)
            .open(path.as_ref())
        {
            Ok(fd) => fd,
            Err(e) if e.kind() == ErrorKind::AlreadyExists => {
                File::options().read(true).write(true).open(path.as_ref())?
            }
            Err(e) => return Err(e),
        };

        let this = PidLock {
            fd,
            pid: Some(pid),
            phantom: PhantomData,
        };

        // wait until this process can acquire an exclusive lock on the pidfile
        let flock = flock {
            // acquire a mutex (write) lock
            l_type: F_WRLCK as _,
            // this triplet of values lock the entire file
            l_whence: SEEK_SET as _,
            l_start: 0,
            l_len: 0,
            // only used by F_GETLK
            l_pid: 0,
        };
        loop {
            // SETLKW should block until the lock can be acquired
            fcntl(this.fd.as_raw_fd(), FcntlArg::F_SETLKW(&flock))?;

            // we acquired a filesystem lock on the pidfile
            // now check whether the referenced process is really dead
            match this.read_pid()? {
                Some(pid) => {
                    // sending a signal of zero only performs existance and permissions checks
                    let ex = unsafe { kill(pid.get() as _, 0) };
                    // continue in the loop, the process still exists
                    if ex == 0 {
                        // TODO check for zombie processes
                        // TODO prevent busy loops if locking is bugged (for example NFS)
                        eprintln!("flock was released, but the pidfile is not stale");
                        continue;
                    }
                    // the target process does not exist, we can write our own pid
                    if errno() == ESRCH {
                        break;
                    }

                    // TODO potentially refine checks for staleness
                    panic!("can neither confirm nor deny staleness of pidfile");
                }
                // the pidfile is empty, we can write our own pid
                None => break,
            }
        }

        this.fd.set_len(0)?;
        this.fd.write_all_at(pid.to_string().as_bytes(), 0)?;

        Ok(this)
    }

    fn read_pid(&self) -> io::Result<Option<NonZeroU32>> {
        // the number of decimal digits needed to represent the largest 32-bit pid
        let mut buf = [0; 10];
        let mut unfilled = &mut buf[..];
        let mut offset = 0;

        while !unfilled.is_empty() {
            match self.fd.read_at(unfilled, offset) {
                Ok(0) => break,
                Ok(n) => {
                    unfilled = &mut unfilled[n..];
                    offset += n as u64;
                }
                Err(e) if e.kind() == ErrorKind::Interrupted => {}
                Err(e) => return Err(e),
            }
        }

        let unfilled_len = unfilled.len();
        let filled = &buf[..buf.len() - unfilled_len];
        if filled.is_empty() {
            return Ok(None);
        }

        let s = str::from_utf8(filled).map_err(Error::other)?;
        Ok(Some(s.parse().map_err(Error::other)?))
    }

    /// Release the lock and report any errors that occur.
    ///
    /// See [the struct level documentation](self) for more.
    pub fn release(mut self) -> io::Result<()> {
        self.release_impl()
    }

    fn release_impl(&mut self) -> io::Result<()> {
        // sanity check: the pidfile still contains our pid
        let pid_owning_lock = self.read_pid()?;
        let our_pid = self
            .pid
            .expect("release cannot be called multiple times")
            .get();
        if pid_owning_lock.map(|opt| opt.get()) != Some(our_pid) {
            eprintln!("at the time of releasing the lock, we no longer seem to control it. refusing to modify the pidfile.");
            return Err(io::Error::other("lock failure"));
        }

        // delete our pid
        self.fd.set_len(0)?;
        self.pid = None;
        Ok(())
    }
}

impl Drop for PidLock {
    /// Same as [`PidLock::release`], but any errors are quietly ignored.
    fn drop(&mut self) {
        if self.pid.is_none() {
            // do nothing, `release()` was already explicitly called
        } else {
            let _ = self.release_impl();
        }
    }
}
